package cat.itb.geobirdwatch.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.geobirdwatch.FirebaseRepository
import cat.itb.geobirdwatch.MainViewModel
import cat.itb.geobirdwatch.R
import cat.itb.geobirdwatch.Tag
import cat.itb.geobirdwatch.databinding.FragmentAddMarkerBinding
import java.text.DecimalFormat

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AddMarkerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddMarkerFragment : Fragment() {
    // TODO: Rename and change types of parameters
    lateinit var binding: FragmentAddMarkerBinding
    private var param1: String? = null
    private var param2: String? = null
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddMarkerBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.currentMarker.observe(viewLifecycleOwner) {
            binding.speciesText.setText(it.species)
            binding.descriptionText.setText(it.description)
            binding.tagSpinner.setSelection(it.tag.ordinal)
            if (it.uploaded) {
                if (it.imagePath.toString() != "") {
                    FirebaseRepository.downloadImage(it.imagePath).addOnCompleteListener {
                        binding.markerImage.setImageBitmap(
                            BitmapFactory.decodeByteArray(
                                it.result,
                                0,
                                it.result.size
                            )
                        )
                    }
                    binding.markerImage.rotation = 90f
                }
            } else {
                binding.markerImage.setImageBitmap(BitmapFactory.decodeFile(it.imagePath.path))
                binding.markerImage.rotation = 90f
            }
        }
        binding.cameraButton.setOnClickListener {
            findNavController().navigate(R.id.action_addMarkerFragment_to_cameraFragment)
        }
        binding.addButton.setOnClickListener {
            val newMarker = viewModel.currentMarker.value!!
            if (newMarker.imagePath.toString() != "") {
                newMarker.species = binding.speciesText.editableText.toString()
                println(binding.speciesText.editableText.toString() + " TEXTBIR")
                newMarker.description = binding.descriptionText.text.toString()
                newMarker.tag = binding.tagSpinner.selectedItem as Tag
                viewModel.saveMarker(newMarker)
            }
        }
        binding.tagSpinner.adapter =
            ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, Tag.values())

        binding.deleteButton.setOnClickListener {
            viewModel.currentMarker.value?.let { it1 -> viewModel.deleteMarker(it1) }
        }

        binding.speciesText.addTextChangedListener {
            viewModel.currentMarker.value?.species = binding.speciesText.editableText.toString()
        }
        binding.descriptionText.addTextChangedListener {
            viewModel.currentMarker.value?.description =
                binding.descriptionText.editableText.toString()
        }


    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddMarkerFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddMarkerFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}