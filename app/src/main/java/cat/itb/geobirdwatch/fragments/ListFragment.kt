package cat.itb.geobirdwatch.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cat.itb.geobirdwatch.*
import cat.itb.geobirdwatch.adapters.MarkerListAdapter
import cat.itb.geobirdwatch.databinding.FragmentMarkerListBinding
import cat.itb.geobirdwatch.model.BirdMarker

class MarkerListFragment : Fragment(), MarkerOnClickListener {
    lateinit var binding: FragmentMarkerListBinding
    private lateinit var MarkerListAdapter: MarkerListAdapter
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMarkerListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.filteredMarkers.value?.let {
            setUpRecyclerView(it)
        }
        viewModel.filteredMarkers.observe(viewLifecycleOwner, Observer {
            if (viewModel.filteredMarkers.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(it)
            }
        })
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!binding.recyclerView.canScrollVertically(1)) {
                    //viewModel.fetchData("next")
                }
            }
        })
        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = false
        }
        binding.tagSpinner.adapter =
            ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, Tag.values())
        binding.tagSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.setTagFilter(binding.tagSpinner.selectedItem as Tag)
            }
        }

    }

    fun setUpRecyclerView(birdMarkers: MutableList<BirdMarker>) {
        MarkerListAdapter = MarkerListAdapter(birdMarkers, this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = MarkerListAdapter
        }
    }


    override fun onClick(marker: BirdMarker) {
        viewModel.currentMarker.postValue(marker)
        (requireActivity() as MainActivity).navigateToFragment(R.id.addMarkerFragment)
    }
}