package cat.itb.geobirdwatch.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import cat.itb.geobirdwatch.FirebaseRepository
import cat.itb.geobirdwatch.MainViewModel
import cat.itb.geobirdwatch.MarkerOnClickListener
import cat.itb.geobirdwatch.R
import cat.itb.geobirdwatch.databinding.MarkerItemBinding
import cat.itb.geobirdwatch.model.BirdMarker

class MarkerListAdapter(
    private val birdMarker_list: MutableList<BirdMarker>,
    private val listenerTag: MarkerOnClickListener
) : RecyclerView.Adapter<MarkerListAdapter.ViewHolder>() {
    private lateinit var context: Context

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = MarkerItemBinding.bind(view)
        fun setListener(birdMarker: BirdMarker) {
            binding.root.setOnClickListener {
                listenerTag.onClick(birdMarker)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.marker_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marker = birdMarker_list[position]
        with(holder) {
            setListener(marker)
            binding.itemSpeciesText.text = marker.species
            binding.itemDescriptionText.text = marker.description
            //binding.image.setImageBitmap(BitmapFactory.decodeFile(marker.imagePath.path))
            FirebaseRepository.downloadImage(marker.imagePath).addOnCompleteListener {
                binding.image.setImageBitmap(
                    BitmapFactory.decodeByteArray(
                        it.result,
                        0,
                        it.result.size
                    )
                )
            }
            binding.image.rotation = 90f
        }
    }

    override fun getItemCount(): Int {
        return birdMarker_list.size
    }


}