package cat.itb.geobirdwatch

enum class Tag {
    Bird,
    Flock,
    Nest,
}