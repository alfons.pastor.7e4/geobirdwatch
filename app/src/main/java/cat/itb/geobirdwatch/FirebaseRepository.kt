package cat.itb.geobirdwatch

import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage

class FirebaseRepository {
    companion object {
        fun downloadImage(uri: Uri): Task<ByteArray> {
            val storage = FirebaseStorage.getInstance().reference
            val pathRef = storage.child(uri.toString())
            val httpsReference = pathRef.getBytes(1024 * 1024)
            return httpsReference
        }
    }
}