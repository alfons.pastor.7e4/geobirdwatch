package cat.itb.geobirdwatch

enum class EditState {
    CREATE, UPDATE
}