package cat.itb.geobirdwatch

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cat.itb.geobirdwatch.model.BirdMarker
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage
import java.lang.Exception

class MainViewModel : ViewModel() {
    val currentMarker = MutableLiveData<BirdMarker>()
    val markers = MutableLiveData<MutableList<BirdMarker>>()
    var filteredMarkers = MutableLiveData<MutableList<BirdMarker>>()
    var currentTagFilter = Tag.values()[0]

    init {
        currentMarker.value = BirdMarker()
        markers.value = mutableListOf()
        val db = FirebaseFirestore.getInstance()

        db.collection("bird_markers").get().addOnSuccessListener {
            val newMarkers: MutableList<BirdMarker> = mutableListOf()
            it.forEach { results ->
                if (results != null) {
                    newMarkers.add(
                        BirdMarker(
                            results.id,
                            results["latitude"] as Double,
                            results["longitude"] as Double,
                            results["species"] as String,
                            results["description"] as String,
                            imagePath = Uri.parse(results["image_path"] as String),
                            Tag.values()[(results["tag"] as Long).toInt()],
                            true
                        )
                    )
                }
            }
            markers.postValue(newMarkers)
        }
    }

    fun downloadImage(uri: Uri): FileDownloadTask {
        val storage = FirebaseStorage.getInstance().reference

        val httpsReference = storage.getFile(uri)
        return httpsReference
    }


    fun setTagFilter(tag: Tag) {
        println("Setting tag filter to $tag")
        currentTagFilter = tag
        updateFilteredMarkers()
    }

    fun updateFilteredMarkers() {
        val filteredMarkers = mutableListOf<BirdMarker>()
        markers.value?.forEach {
            if (it.tag == currentTagFilter) {
                filteredMarkers.add(it)
            }
        }

        this.filteredMarkers.postValue(filteredMarkers)
        println("Filtered markers: $filteredMarkers")
    }

    fun saveMarker(birdMarker: BirdMarker){
        if(birdMarker.id == ""){
            addMarker(birdMarker)
        }
        else{
            updateMarker(birdMarker)
        }
    }

    fun updateMarker(birdMarker: BirdMarker){
        val db = FirebaseFirestore.getInstance()

        val file = birdMarker.imagePath
        val storage = FirebaseStorage.getInstance().reference

        val childRef = storage.child("images/${file.lastPathSegment}")

        val uploadTask = childRef.putFile(file)
        val urlTask = uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            childRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isComplete) {
                birdMarker.imagePath = Uri.parse("images/${file.lastPathSegment}")

                db.collection("bird_markers").document(birdMarker.id).set(
                    hashMapOf(
                        "latitude" to birdMarker.latitude,
                        "longitude" to birdMarker.longitude,
                        "species" to birdMarker.species,
                        "description" to birdMarker.description,
                        "image_path" to birdMarker.imagePath.toString(),
                        "tag" to birdMarker.tag.ordinal
                    )
                ).addOnCompleteListener {
                    birdMarker.uploaded = true
                    //markers.value?.add(birdMarker.copy())
                    updateFilteredMarkers()
                }
            }
        }
    }

    fun addMarker(birdMarker: BirdMarker) {
        val db = FirebaseFirestore.getInstance()

        val file = birdMarker.imagePath
        val storage = FirebaseStorage.getInstance().reference

        val childRef = storage.child("images/${file.lastPathSegment}")

        val uploadTask = childRef.putFile(file)
        val urlTask = uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            childRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isComplete) {
                birdMarker.imagePath = Uri.parse("images/${file.lastPathSegment}")
                db.collection("bird_markers").add(
                    hashMapOf(
                        "latitude" to birdMarker.latitude,
                        "longitude" to birdMarker.longitude,
                        "species" to birdMarker.species,
                        "description" to birdMarker.description,
                        "image_path" to birdMarker.imagePath.toString(),
                        "tag" to birdMarker.tag.ordinal
                    )
                ).addOnCompleteListener {
                    birdMarker.id = it.result.id
                    birdMarker.uploaded = true
                    markers.value?.add(birdMarker.copy())
                    updateFilteredMarkers()
                }
            }
        }

    }

    fun deleteMarker(birdMarker: BirdMarker) {
        val db = FirebaseFirestore.getInstance()
        try {
            db.collection("bird_markers").document(birdMarker.id).delete()
            val newMarkers = markers.value
            newMarkers?.remove(birdMarker)
            markers.postValue(newMarkers!!)
        }
        catch (e: Exception){
            println(e)
        }

    }

}