package cat.itb.geobirdwatch.model

import android.net.Uri
import androidx.core.net.toUri
import cat.itb.geobirdwatch.Tag

data class BirdMarker(
    var id: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var species: String = "",
    var description: String = "",
    var imagePath: Uri = "".toUri(),
    var tag: Tag = Tag.Bird,
    var uploaded: Boolean = false
)