package cat.itb.geobirdwatch

import cat.itb.geobirdwatch.model.BirdMarker

interface MarkerOnClickListener {
    fun onClick(birdMarker: BirdMarker)

}